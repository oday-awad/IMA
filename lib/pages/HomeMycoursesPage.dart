import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ima/classes/Categories.dart';
import 'package:ima/classes/Courses.dart';
import 'package:ima/classes/Sections.dart';

class MyCoursesPage extends StatefulWidget {
  @override
  _MyCoursesPageState createState() => _MyCoursesPageState();
}

class _MyCoursesPageState extends State<MyCoursesPage>
    with SingleTickerProviderStateMixin {
  Size size ;

  List<int> myCourses = [1, 1, 1, 1, 1];
  List<String> images = ['python.png', 'belgets.jpg','python.png', 'belgets.jpg'];

  List<Categories> categories = <Categories>[];
  List<Sections> sections = <Sections>[];
  List<Courses> courses = <Courses>[];

  bool _fabVisible;

  TabController _tabController;
  ScrollController _scrollController;

  @override
  void initState() {
    _fabVisible = true;
    _tabController = TabController(length: 2, vsync: this)
      ..addListener(() {
        setState(() {
          if (_tabController.previousIndex == 1)
            _fabVisible = true;
          else
            _fabVisible = false;
        });
      });

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    _scrollController = ScrollController()
      ..addListener(() {
        setState(() {
          if (_scrollController.position.userScrollDirection ==
              ScrollDirection.forward)
            _fabVisible = true;
          else
            _fabVisible = false;
        });
      });

    super.initState();
  }

  getCategories() {
    categories.clear();
    Categories a1 = new Categories(
        name: 'Programming', imageURL: 'assets/svgImages/catagory.svg');
    Categories a2 = new Categories(
        name: 'Designing', imageURL: 'assets/svgImages/check (1).svg');
    Categories a3 = new Categories(
        name: 'Data Mining', imageURL: 'assets/svgImages/key.svg');
    Categories a4 = new Categories(
        name: 'AI', imageURL: 'assets/svgImages/catagory.svg');
    Categories a5 = new Categories(
        name: 'Social Media', imageURL: 'assets/svgImages/catagory.svg');

    categories.add(a1);
    categories.add(a2);
    categories.add(a3);
    categories.add(a4);
    categories.add(a5);
  }

  getSections() {
    sections.clear();
    sections.add(Sections(name: 'Pupular Courses'));
    sections.add(Sections(name: 'New Courses'));
    sections.add(Sections(name: 'Trend Courses'));
  }

  getCourses() {
    courses.clear();
    courses.add(Courses(
        name: 'Python',
        imageURL: 'assets/images/python.png',
        newPrice: 199,
        oldPrice: 600,
        rate: 4,
        unit: 'EGP'));
    courses.add(Courses(
        name: 'javaScript',
        imageURL: 'assets/images/python.png',
        newPrice: 50,
        oldPrice: 100,
        rate: 2,
        unit: 'EGP'));
    courses.add(Courses(
        name: 'Flutter',
        imageURL: 'assets/images/python.png',
        newPrice: 30,
        oldPrice: 60,
        rate: 7,
        unit: 'EGP'));
    courses.add(Courses(
        name: 'Dart',
        imageURL: 'assets/images/python.png',
        newPrice: 50,
        oldPrice: 100,
        rate: 3,
        unit: 'EGP'));
    courses.add(Courses(
        name: 'PHP',
        imageURL: 'assets/images/python.png',
        newPrice: 50,
        oldPrice: 100,
        rate: 3,
        unit: 'EGP'));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getCategories();
    getSections();
    getCourses();

    size = MediaQuery.of(context).size;
    FlutterStatusbarcolor.setStatusBarColor(Color(0xff202D5A));

    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        iconTheme: IconThemeData(color: Colors.black),
        actionsIconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/SearchPage');
            },
            icon: Icon(Icons.search),
          )
        ],
        centerTitle: true,
        toolbarOpacity: 1,
        backgroundColor: Colors.white,
        title: Text(
          'IMA',
          style: TextStyle(
            color: Colors.black,
            fontSize: 19,
            fontWeight: FontWeight.w600,
          ),
        ),
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Expanded(
                child: Container(
                  child: TabBarView(
                    children: [
                      buildHome(),
                      buildMyCourses(),
                    ],
                    controller: _tabController,
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(bottom: 4),
            decoration: BoxDecoration(
              color: const Color(0xffffffff),
              boxShadow: [
                BoxShadow(
                  offset: const Offset(0, 2),
                  blurRadius: 6,
                  color: const Color(0xff000000).withOpacity(0.25),
                )
              ],
            ),
            child: TabBar(
              controller: _tabController,
              indicatorColor: Color(0xff202D5A),
              indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(width: 2.0),
                  insets: EdgeInsets.symmetric(horizontal: 18.0)),
              tabs: [
                Container(
                  color: Colors.white,
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Home',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'My Courses',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ],
              onTap: (i) {
                if (i == 0) {
                  setState(() {
                    _fabVisible = true;
                  });
                } else {
                  setState(() {
                    _fabVisible = false;
                  });
                }
              },
            ),
          ),
        ],
      ),
      floatingActionButton: SpeedDial(
        backgroundColor: Color(0xff202D5A),
        onPress: () {
          Navigator.of(context).pushNamed('/CartPage');
        },
        curve: Curves.bounceIn,
        visible: _fabVisible,
        child: Icon(
          Icons.shopping_cart,
          color: Colors.white,
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Color(0xff202D5A),
              ),
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 89,
                      width: 89,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset(
                          'assets/images/belgets.jpg',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/SigninSignupPage');
                      },
                      child: Text('signup or signin',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          )),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  buildHome() {
    return Container(
      color: Colors.white,
      child: ListView(
        controller: _scrollController,
        padding: EdgeInsets.only(bottom: 8),
        children: <Widget>[
          CarouselSlider.builder(
            options: CarouselOptions(
              autoPlay: true,
              viewportFraction: 1.0,
              height: 175,
            ),
            itemCount: images.length,
            itemBuilder: (BuildContext context, int itemIndex) => Row(
              children: [
                Container(
                  width: size.width,
                  child: Image.asset(
                    'assets/images/${images[itemIndex]}',
                    fit: BoxFit.cover,
                    width: 300,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
            width: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              'Catagories',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 20,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            height: 160,
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: categoriesBuilder,
              itemCount: categories.length,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            children: sections
                .map((section) => sectionsBuilder(context, section))
                .toList(),
          )
        ],
      ),
    );
  }

  Widget sectionsBuilder(BuildContext context, Sections section) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Text(
                '${section.name}',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 0),
              child: FlatButton(
                onPressed: () {
                  //todo see all function
                },
                child: Text(
                  'see all',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Color(0xff344FAC),
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 4,
        ),
        Container(
          height: 207,
          child: ListView.builder(
            itemBuilder: courseBuilder,
            itemCount: courses.length,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  buildMyCourses() {
    return Container(
      color: Colors.white,
      child: ListView.builder(
        itemCount: myCourses.length,
        itemBuilder: myCourseBuilder,
      ),
    );
  }

  Widget myCourseBuilder(BuildContext context, int index) {
    return Container(
      height: 250,
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
      ),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 126,
              width: size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(13),
                      topRight: Radius.circular(13))),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(13),
                    topLeft: Radius.circular(13)),
                child: Image.asset(
                  'assets/images/python.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Text(
                'Python',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 21),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: LinearProgressIndicator(
                      value: 0.5,
                      backgroundColor: Color(0xff8E9CCC),
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xff202D5A)),
                    ),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Text(
                    '50%',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'left time',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Text(
                    '20:15',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Expanded(child: Container()),

                     RaisedButton(
                      color: Color(0xff202D5A),
                      onPressed: () {
                        Navigator.of(context).pushNamed('/MyCourseContentPage');
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),

                        child: Text(
                          'resume',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),

                    ),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget categoriesBuilder(BuildContext context, int index) {
    return Container(
      width: 150,
      padding: EdgeInsets.only(right: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 125,
            height: 125,
            child: RaisedButton(
              padding: EdgeInsets.all(8),
              onPressed: () {
                Navigator.of(context).pushNamed('/CatagoryPage').then((_) {
                  setState(() {
                    SystemChrome.setPreferredOrientations([
                      DeviceOrientation.portraitUp,
                      DeviceOrientation.portraitDown,
                    ]);
                  });
                });
              },
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(75),
              ),
              color: Colors.white,
              //todo replace with network image
//              child: SvgPicture.network('${categories[index].imageURL}'),
              child: SvgPicture.asset('${categories[index].imageURL}'),
//              child: SvgPicture.asset('assets/svgImages/catagory.svg'),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            '${categories[index].name}',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
            maxLines: 2,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  Widget courseBuilder(BuildContext context, int index) {
    return Container(
      width: 225,
      height: 200,
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: RaisedButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/CourseDetailesPage');
        },
        color: Colors.white,
        elevation: 2,
        padding: EdgeInsets.all(0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(13)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 100,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(13),
                    topLeft: Radius.circular(13)),
                child: Image.asset(
                  '${courses[index].imageURL}',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Text(
                '${courses[index].name}',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              height: 9,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: starsBuilder(courses[index].rate),
              ),
            ),
            SizedBox(
              height: 9,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${courses[index].newPrice} ${courses[index].unit}',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Color(0xff344FAC),
                        fontSize: 16),
                  ),
                  SizedBox(
                    width: 7,
                  ),
                  Text(
                    '${courses[index].oldPrice} ${courses[index].unit}',
                    style: TextStyle(
                        decoration: TextDecoration.lineThrough,
                        fontWeight: FontWeight.w600,
                        fontSize: 16),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  List<Widget> starsBuilder(int n) {
    List<Widget> stars = [];
    for (int i = 0; i < 5; i++) {
      if (i  < n)
        stars.add(Icon(
          Icons.star,
          color: Colors.amber,
        ));
      else
        stars.add(Icon(
          Icons.star,
          color: Colors.black54,
        ));
    }
    return stars;
  }
}
